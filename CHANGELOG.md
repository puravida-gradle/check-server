# Changelog

Changelog of Puravida Gradle Extension.

## Unreleased
### No issue

**add generate changelog in release process**


[b0ed1104036b1ca](https://gitlab.com/puravida-gradle/check-server/commit/b0ed1104036b1ca) jorge *2018-09-12 21:18:39*


## v1.0.0
### No issue

**set dependencies for release plugin**


[f0c0c8a16e44ad5](https://gitlab.com/puravida-gradle/check-server/commit/f0c0c8a16e44ad5) jorge *2018-09-12 21:07:30*

**add bintray to pipeline**


[0106d3c2784a8e9](https://gitlab.com/puravida-gradle/check-server/commit/0106d3c2784a8e9) jorge *2018-09-12 20:40:02*

**improve documentation with downloadFile**


[8701527f1ab9fc7](https://gitlab.com/puravida-gradle/check-server/commit/8701527f1ab9fc7) jorge *2018-09-12 20:38:56*

**improve documentatio with downloadFile**


[f1bf2d365676341](https://gitlab.com/puravida-gradle/check-server/commit/f1bf2d365676341) jorge *2018-09-12 20:38:39*

**use domain name for plugin**


[2a18a585b5d075c](https://gitlab.com/puravida-gradle/check-server/commit/2a18a585b5d075c) jorge *2018-09-12 20:29:12*

**improve task**


[b147b9363f09660](https://gitlab.com/puravida-gradle/check-server/commit/b147b9363f09660) jorge *2018-09-12 20:25:42*

**decrease gradle version**


[310b7df03a4913c](https://gitlab.com/puravida-gradle/check-server/commit/310b7df03a4913c) jorge *2018-09-12 20:25:33*

**add bintray**


[b600ca2089eff11](https://gitlab.com/puravida-gradle/check-server/commit/b600ca2089eff11) jorge *2018-09-12 20:25:12*

**add some usefull plugins as changelog and git release**


[8f1d95779a8e75c](https://gitlab.com/puravida-gradle/check-server/commit/8f1d95779a8e75c) jorge *2018-09-09 10:21:28*


## 1.0.0-beta1
### No issue

**ignore intellij**


[4c84bada3405855](https://gitlab.com/puravida-gradle/check-server/commit/4c84bada3405855) jorge *2018-09-09 08:58:57*

**typo in documentation**


[4ef56c21acd5df7](https://gitlab.com/puravida-gradle/check-server/commit/4ef56c21acd5df7) jorge *2018-09-01 08:35:02*

**rename pluginId as namespace to be able to publish it**


[f61a5692b74201d](https://gitlab.com/puravida-gradle/check-server/commit/f61a5692b74201d) jorge *2018-09-01 08:15:31*

**improve performance in CI**


[baba2eb887f6480](https://gitlab.com/puravida-gradle/check-server/commit/baba2eb887f6480) jorge *2018-09-01 08:08:01*

**publish with gitlab and docu**


[4ddf5e8b9073557](https://gitlab.com/puravida-gradle/check-server/commit/4ddf5e8b9073557) jorge *2018-09-01 08:02:48*

**publish with gitlab and docu**


[b2be0263c659bbe](https://gitlab.com/puravida-gradle/check-server/commit/b2be0263c659bbe) jorge *2018-09-01 07:53:22*

**add repositories**


[cd57784dde52930](https://gitlab.com/puravida-gradle/check-server/commit/cd57784dde52930) jorge *2018-08-31 15:03:08*

**first commit**


[35a1f994769406d](https://gitlab.com/puravida-gradle/check-server/commit/35a1f994769406d) jorge *2018-08-31 14:59:17*


