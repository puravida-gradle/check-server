package com.puravida.gradle.checkserver

import groovy.util.logging.Log
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

@Log
class CheckServerTask extends DefaultTask{

    CheckServerTask(){
        enabled = !project.gradle.startParameter.offline
    }


    private Object urlObject

    private Object dumpObject

    @Input
    String getUrl() {
        urlObject.toString()
    }

    void setUrl(Object url){
        this.urlObject = url
    }

    void url(Object url){
        this.urlObject = url
    }

    @OutputFile
    @Optional
    File getDumpFile() {
        dumpObject ? project.file(dumpObject) : null
    }

    void setDumpFile(Object dumpFile){
        this.dumpObject=dumpFile
    }

    void dumpFile(Object dumpFile){
        this.dumpObject=dumpFile
    }


    @TaskAction
    void callYourMethodAsYouWant(){
        try {
            assert urlObject, "a URL must to be provider"

            if(dumpFile) {
                File output = dumpFile
                output.bytes = urlObject.toURL().bytes
            }else
                urlObject.toURL().bytes

        }catch(Exception e){
            log.severe(e.message)
            throw new TaskExecutionException(this,new Exception("Exception occured while processing checkServerTask",e));
        }
    }


}
