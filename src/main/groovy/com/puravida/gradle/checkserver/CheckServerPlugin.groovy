package com.puravida.gradle.checkserver

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration

class CheckServerPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.with {
            apply(plugin: 'base')
            task('checkServer', type:CheckServerTask, group:'others',description: 'Look Mom, a plugin from Puravida in my Gradle'){

            }
        }
    }

}
