package com.puravida.gradle.checkserver

import groovy.util.logging.Log
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.FAILED

class CheckServerTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.checkserver'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {
        buildFile << """
            checkServer{
                url 'http://www.google.com'                
            }
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('checkServer', '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":checkServer").outcome == SUCCESS
    }


    def "run full example task"() {
        buildFile << """

// tag::executeTask[]
            checkServer{
                url 'http://www.google.com'     //<1>
                dumpFile 'build/checkserver.html' //<2>                
            }
// end::executeTask[]
            
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('checkServer', '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":checkServer").outcome == SUCCESS

        and:
        new File( new File(testProjectDir.root,"build"),"checkserver.html").exists()
    }

    def "run failed task"() {
        buildFile << """

// tag::executeFailedTask[]
            checkServer{
                url 'http://this.host.doesnt.exist'                
            }
// end::executeFailedTask[]
            
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('checkServer', '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .buildAndFail()

        then:
        result.task(":checkServer").outcome == FAILED
    }
}
