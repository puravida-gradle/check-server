package com.puravida.gradle.checkserver


import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.DependencyResolutionListener
import org.gradle.api.artifacts.ResolvableDependencies
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Ignore
import spock.lang.Specification

class PluginSpec  extends Specification{

    Project project

    def setup() {
        project = ProjectBuilder.builder().build()
    }

    @SuppressWarnings('MethodName')
    def "Applies plugin and checks default setup"() {

        expect:
        project.tasks.findByName("checkServer") == null

        when:
        project.apply plugin: 'com.puravida.gradle.checkserver'

        then:
        Task checkServer = project.tasks.findByName('checkServer')
        checkServer != null

        project.tasks.findByName('clean') != null
    }

}
